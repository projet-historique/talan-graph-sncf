#!/bin/sh

set -e

sleep 5
uwsgi --wsgi-file "/home/sncf/wsgi.py" --http-socket 0.0.0.0:6080 --chdir "/home/" --show-config

