FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

COPY . /home/
RUN chmod +x /home/entrypoint.sh

# Install packages and dependencies
ADD ./requirements.txt /requirements.txt

# Install pip for Python 3.7
RUN apk update && apk upgrade
RUN apk add python3-dev build-base linux-headers pcre-dev libressl curl
RUN apk add gcc python3-dev musl-dev

RUN pip3 install -U pip
RUN LIBRARY_PATH="/lib:/usr/lib"
RUN /bin/sh -c "pip3 install -r /requirements.txt --no-cache-dir"

WORKDIR /home/

EXPOSE 6080

ENTRYPOINT ["/home/entrypoint.sh"]
