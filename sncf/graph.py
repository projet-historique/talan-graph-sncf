BASE_DIR = "/tmp/"

class Graph:
    """ Simple class for graph: adjacency lists

    Attributes:
        order (int): Number of nodes.
        directed (bool): True if the graph is directed. False otherwise.
        adjlists (List[List[int]]): Lists of connected vertices for each vertex.

    """

    def __init__(self, order=25000, directed=False, labels=None, costs=False):
        """Init graph, allocate adjacency lists

        Args:
            order (int): Number of nodes.
            directed (bool): True if the graph is directed. False otherwise.
            costs (bool): True if the graph is weighted. False otherwise.

        """

        self.order = order
        self.directed = directed
        if costs:
            self.costs = {}
        else:
            self.costs = None
        self.adjlists = []
        for _ in range(order):
            self.adjlists.append([])

        self.labels = labels
        self.nodes = []
        self.dist = None
        self.p = None

    def addedge(self, src, dst, cost=None, multi=True):
        """Add egde to graph.

        Args:
            src (int): Source vertex.
            dst (int): Destination vertex.
            cost: if not None, the cost of edge (src, dst)
            multi (bool): True for multigraphs

        Raises:
            IndexError: If any vertex index is invalid.
            Exception: If graph is None.

        """

        # Check graph and vertex indices.
        if self is None:
            raise Exception('Empty graph')
        if src >= self.order or src < 0:
            raise IndexError("Invalid src index")
        if dst >= self.order or dst < 0:
            raise IndexError("Invalid dst index")
        # Add edge if multi or not already existing, and reverse-edge if undirected.
        if multi or (src != dst and dst not in self.adjlists[src]):
            self.adjlists[src].append(dst)
            if not self.directed and dst != src:
                self.adjlists[dst].append(src)
        if self.costs != None:
            self.costs[(src, dst)] = cost
            if not self.directed:
                self.costs[(dst, src)] = cost

    def removeedge(self, src, dst):
        """Remove egde from the graph.

        Args:
            src (int): Source vertex.
            dst (int): Destination vertex.

        Raises:
            IndexError: If any vertex index is invalid.
            Exception: If graph is None.

        """

        # Check graph and vertex indices.
        if self is None:
            raise Exception('Empty graph')
        if src >= self.order or src < 0:
            raise IndexError("Invalid src index")
        if dst >= self.order or dst < 0:
            raise IndexError("Invalid dst index")
        # Add edge if multi or not already existing, and reverse-edge if undirected.
        if dst in self.adjlists[src]:
            self.adjlists[src].remove(dst)
            if self.costs:
                self.costs.pop((src, dst))
            if not self.directed and dst != src:
                self.adjlists[dst].remove(src)
                if self.costs:
                    self.costs.pop((dst, src))



def load_weightedgraph(filename, costType=float):
    """Build a new weighted graph from a WGRA file.

    Args:
        filename (str): File to load.

    Returns:
        Graph: New graph.
    """
    f = open("/home/" + filename)
    lines = f.readlines()
    infos = {}
    i = 0

    directed = 0
    order = 0
    G = Graph(30000, directed, costs=True)
    G.infos = infos
    for line in lines[1:]:
        edge = line.strip().split(';')
        tmp = ""
        for i in range(len(edge[2])):
            if edge[2][i] != ',':
                tmp += edge[2][i]
            else:
                tmp += '.'
        edge[2] = tmp
        (x, y, cost) = (str(edge[0]), str(edge[1]), costType(edge[2]))

        if not x in G.nodes:
            G.nodes.append(x)
        if not y in G.nodes:
            G.nodes.append(y)
        G.addedge(G.nodes.index(x), G.nodes.index(y), cost)
    f.close()
    return G


inf = float('inf')

def Floyd(G):
    n = len(G.nodes)
    dist = []
    p = []
    for x in range(n):
        dist.append([])
        p.append([])
        for y in range(n):
            if (x, y) in G.costs:
                dist[x].append(G.costs[(x, y)])
            else:
                dist[x].append(inf)
            p[x].append(y)

    for i in range(n):
        for x in range(n):
            for y in range(n):
                if dist[x][i] + dist[i][y] < dist[x][y]:
                    dist[x][y] = dist[x][i] + dist[i][y]
                    p[x][y] = p[i][y]

    G.p = p
    G.dist = dist

def search_pcc(G, PR_debut, PR_fin):
    begin = G.nodes.index(PR_debut)
    end = G.nodes.index(PR_fin)
    return G.dist[begin][end]

def convert_to_csv(G):
    file = open("/media/Talan/output.csv", mode='w')
    long = len(G.nodes)
    line = "PR_Debut;PR_Fin;Distance\r\n"
    file.write(line)
    for i in range(long):
        line = ""
        for j in range(long):
            line += str(G.nodes[i]) + ';'
            line += str(G.nodes[j]) + ';'
            if G.nodes[i] == G.nodes[j]:
                line += str(float(0)) + '\r\n'
            else:
                line += str(G.dist[i][j]) + '\r\n'
        file.write(line)
    file.close()
    return "https://static.hyperion.tf/Talan/output.csv"
