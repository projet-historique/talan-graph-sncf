from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from . import graph
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from .settings import TEMPLATE_DIRS


@login_required(login_url="/login/")
def index(request):
    if request.POST:
        file = request.FILES['file']
        fs = FileSystemStorage()
        filename = fs.save("idf_db.csv", file)
        pathfile = fs.url(filename)
        G = graph.load_weightedgraph(pathfile)
        graph.Floyd(G)
        output = graph.convert_to_csv(G)
        return render(request, 'upload_done.html', locals())
    return render(request, 'index.html', locals())

def login_view(request):
    form = None
    if request.POST:
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/')
    else:
        form = AuthenticationForm()
    print(TEMPLATE_DIRS)
    return render(request,'login.html', locals())

def logout_view(request):
    logout(request)
    return render(request, 'logout.html', locals())
